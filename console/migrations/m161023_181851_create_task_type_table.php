<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_type`.
 */
class m161023_181851_create_task_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task_type');
    }
}
