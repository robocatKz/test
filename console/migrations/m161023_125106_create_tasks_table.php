<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m161023_125106_create_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'author' => $this->string(255)->notNull(),
            'executor_of' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->string(255),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'start_date' => $this->dateTime(),
            'finish_date' => $this->dateTime(),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tasks');
    }
}
