<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Type;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\TasksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // echo$form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'type_id') ?>
    <?php 
            $types = Type::find()->all();
            $type = ArrayHelper::map($types, 'id', 'name');
            $params = [
                'prompt' => ' ',
           ];
            echo $form->field($model, 'type_id')->dropDownList($type, $params);
    ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'author') ?>

    <?php // echo $form->field($model, 'executor_of') ?>

    <?php // echo $form->field($model, 'status') ?>
    
    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'finish_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
