<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Type */

$this->title = 'Update Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
    	'class' => 'btn btn-danger',
    	'data' => [
        		'confirm' => 'Are you sure you want to delete this item?',
        		'method' => 'post',
    	],
     ]) ?>

</div>
