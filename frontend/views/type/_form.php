<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Type */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
            $items = [
                1 => 'Да',
                0 => 'Нет',
            ];
            echo $form->field($model, 'is_active')->dropDownList($items);
    ?>

    <?php
            $default = [
                1 => 'Да',
                0 => 'Нет',
            ];
            echo $form->field($model, 'is_default')->dropDownList($default);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'create_date')->textInput() ?>

    <?php // echo $form->field($model, 'update_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
