<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\Tasks;

/**
 * This is the model class for table "task_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $is_active
 * @property string $create_date
 * @property string $update_date
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_active', 'is_default'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'is_active' => 'Активность',
            'is_default' => 'По умолчанию',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
        ];
    }

    /**
     * @inheritdoc
     * @return TypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TypeQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function  beforeDelete()
    {
        if($this->is_default == 1){
            Yii::trace('Установите тип по умолчанию');
        } else {  
            $type= Type::find()->where(['is_default' => 1])->one();
            Tasks::updateAll(['type_id'=>$type->id], ['type_id'=>$this->id]);  
        }
        return parent::beforeDelete();
    }
}
