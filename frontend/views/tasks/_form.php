<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 
use common\models\User;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use common\models\Tasks;
use common\models\Type;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
            $types = Type::find()->all();
            $type = ArrayHelper::map($types, 'id', 'name');
            echo $form->field($model, 'type_id')->dropDownList($type);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php 
            //$author = Yii::$app->user->id;
            $author_name = Yii::$app->user->identity->username;
            echo $form->field($model, 'author')->textarea(['value' => $author_name, 'readonly' => true ]) 
    ?>

    <?php
            $users = User::find()->all();
            $user = ArrayHelper::map($users, 'id', 'username');
            echo $form->field($model, 'executor_of')->dropDownList($user);
    ?>

    <?php
            $items = [
                Tasks::STATUS_BACKLOG => 'Бэклог',
                Tasks::STATUS_SFD => 'Выбрать для разработки',
                Tasks::STATUS_INPROGRES => 'В работе',
                Tasks::STATUS_REVIEW => 'Для проверки',
                Tasks::STATUS_DONE=> 'Готово',
            ];
            echo $form->field($model, 'status')->dropDownList($items);
    ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Дата публикации ...'],
        'language' => 'ru',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss'
        ]
     ]); ?>

    <?= $form->field($model, 'finish_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Дата публикации ...'],
        'language' => 'ru',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss'
        ]
     ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
