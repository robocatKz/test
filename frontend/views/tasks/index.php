<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tasks', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            'description',
            'author',
            //'executor_of',
            [
                'label' => 'Кого назначить',
                'value' => 'user.username',
            ],
            'status',
            [
                'attribute' => 'type.name',
                'format' => 'text',
                'label' => 'Тип задач'
            ],
            // 'create_date',
            // 'update_date',
            // 'start_date',
            // 'finish_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

     ?>


</div>
