<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=test80lvl',
            'username' => 'root',
            'password' => 'hgfgbgtg99bu',
            'charset' => 'utf8',
        ],
    ],
];
