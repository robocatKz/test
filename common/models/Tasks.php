<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $author
 * @property string $executor_of
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property string $start_date
 * @property string $finish_date
 * @property string $create_date
 * @property string $update_date
 */
class Tasks extends \yii\db\ActiveRecord
{

    const  STATUS_BACKLOG  =1;
    const STATUS_SFD = 2;
    const STATUS_INPROGRES = 3;
    const STATUS_REVIEW = 4;
    const STATUS_DONE= 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'status'], 'integer'],
            [['author', 'executor_of', 'title'], 'required'],
            [['start_date', 'finish_date', 'create_date', 'update_date'], 'safe'],
            [['author', 'executor_of', 'title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Тип задач',
            'author' => 'Автор',
            'executor_of' => 'Кого назначить',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'status' => 'Статус',
            'start_date' => 'Start Date',
            'finish_date' => 'Finish Date',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'executor_of']);
    }

    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

}
