<?php

use yii\db\Migration;

class m161102_115153_add_column_type extends Migration
{
    public function up()
    {
        $this->addColumn('task_type', 'is_default', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('task_type','is_default');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
